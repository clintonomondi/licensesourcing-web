<!DOCTYPE html>
<html lang="en">
<head>

   <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>License</title>
    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Buy software License and IT Support. "/>
    <meta property="og:image" content="/assets/img/12.png"/>
    <meta property="og:description" content="Reliable system for license sales automation and shipping of digital license."/>

 <!-- Favicons -->
    <link href="/assets/img/12.png" rel="icon">
    <link href="/assets/img/12.png" rel="apple-touch-icon">

    <link href="/assets/css/theme.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/mpesa.css" rel="stylesheet" type="text/css" />
    <link href="/css/search.css" rel="stylesheet" type="text/css" />
    <link href="/css/cart.css" rel="stylesheet" type="text/css" />
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,700&display=swap" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cantarell&display=swap" rel="stylesheet">
</head>
<body>
{{-- <div class="loader">--}}
{{--    <div class="loading-animation"></div>--}}
{{--</div>--}}
<div class="content2">
<div id="app">
    <app></app>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</div>
 <footer class="bg-primary-3 text-white links-white pb-4 footer-1">
     <div class="container">
         <div class="row justify-content-center">

         <div class="row flex-column flex-lg-row align-items-center justify-content-center justify-content-center text-center ">
             <div class="col-auto">
                 <div class="d-flex flex-column flex-sm-row align-items-center text-small">
                     <div class="text-muted">&copy; Copyright 2021-Detaline Ventures Limited  All right reserved <a href="/policy">Privacy Policy</a>
                     </div>
                 </div>
             </div>
             <div class="col-auto mt-3 mt-lg-0">
                 <ul class="list-unstyled d-flex mb-0">
                     <li class="mx-3">
                         <a href="#" class="hover-fade-out">
                             <img src="assets/img/icons/social/twitter.svg" alt="Twitter" class="icon icon-xs bg-white" data-inject-svg>
                         </a>
                     </li>
                 </ul>
             </div>
         </div>
     </div>
     </div>
 </footer>
 <a href="#top" class="btn btn-primary rounded-circle btn-back-to-top" data-smooth-scroll data-aos="fade-up" data-aos-offset="2000" data-aos-mirror="true" data-aos-once="false">
     <img src="/assets/img/icons/interface/icon-arrow-up.svg" alt="Icon" class="icon bg-white" data-inject-svg>
 </a>

<!-- Required vendor scripts (Do not remove) -->
<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/popper.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.js"></script>

<!--
         This appears in the demo only.  This script ensures our demo countdowns are always showing a date in the future
         by altering the date before the countdown is initialized
    -->

<!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

<!-- AOS (Animate On Scroll - animates elements into view while scrolling down) -->
<script type="text/javascript" src="/assets/js/aos.js"></script>
<!-- Clipboard (copies content from browser into OS clipboard) -->
<script type="text/javascript" src="/assets/js/clipboard.min.js"></script>
<!-- Fancybox (handles image and video lightbox and galleries) -->
<script type="text/javascript" src="/assets/js/jquery.fancybox.min.js"></script>
<!-- Flatpickr (calendar/date/time picker UI) -->
<script type="text/javascript" src="/assets/js/flatpickr.min.js"></script>
<!-- Flickity (handles touch enabled carousels and sliders) -->
<script type="text/javascript" src="/assets/js/flickity.pkgd.min.js"></script>
<!-- Ion rangeSlider (flexible and pretty range slider elements) -->
<script type="text/javascript" src="/assets/js/ion.rangeSlider.min.js"></script>
<!-- Isotope (masonry layouts and filtering) -->
<script type="text/javascript" src="/assets/js/isotope.pkgd.min.js"></script>
<!-- jarallax (parallax effect and video backgrounds) -->
<script type="text/javascript" src="/assets/js/jarallax.min.js"></script>
<script type="text/javascript" src="/assets/js/jarallax-video.min.js"></script>
<script type="text/javascript" src="/assets/js/jarallax-element.min.js"></script>
<!-- jQuery Countdown (displays countdown text to a specified date) -->
<script type="text/javascript" src="/assets/js/jquery.countdown.min.js"></script>
<!-- jQuery smartWizard facilitates steppable wizard content -->
<script type="text/javascript" src="/assets/js/jquery.smartWizard.min.js"></script>
<!-- Plyr (unified player for Video, Audio, Vimeo and Youtube) -->
<script type="text/javascript" src="/assets/js/plyr.polyfilled.min.js"></script>
<!-- Prism (displays formatted code boxes) -->
<script type="text/javascript" src="/assets/js/prism.js"></script>
<!-- ScrollMonitor (manages events for elements scrolling in and out of view) -->
<script type="text/javascript" src="/assets/js/scrollMonitor.js"></script>
<!-- Smooth scroll (animation to links in-page)-->
<script type="text/javascript" src="/assets/js/smooth-scroll.polyfills.min.js"></script>
<!-- SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles)-->
<script type="text/javascript" src="/assets/js/svg-injector.umd.production.js"></script>
<!-- TwitterFetcher (displays a feed of tweets from a specified account)-->
<script type="text/javascript" src="/assets/js/twitterFetcher_min.js"></script>
<!-- Typed text (animated typing effect)-->
<script type="text/javascript" src="/assets/js/typed.min.js"></script>
<!-- Required theme scripts (Do not remove) -->
<script type="text/javascript" src="/assets/js/theme.js"></script>
<script type="text/javascript" src="/js/system.js"></script>
<!-- This script appears only on the demo.  It is used to delay unnecessary image loading until after the main page content is loaded. -->

<script src="/loader/center-loader.js"></script>

</body>
</html>
