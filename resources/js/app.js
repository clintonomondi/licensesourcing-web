import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment';
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import vSelect from "vue-select";
import CoolLightBox from 'vue-cool-lightbox'
import 'vue-cool-lightbox/dist/vue-cool-lightbox.min.css'
import shareIt from 'vue-share-it';



Vue.use(shareIt);
Vue.use(require('vue-script2'));
Vue.use(CoolLightBox);
Vue.component("v-select", vSelect);
import GoogleAuth from 'vue-google-oauth2'
const gauthOption = {
    clientId: '30173417980-1mr78rb4ietprqboovvm5j9hg3265fj2.apps.googleusercontent.com',
    scope: 'profile email',
    prompt: 'select_account',
};
Vue.use(GoogleAuth, gauthOption);

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);


import App from './view/App'
import Index from './pages/index'
import Products from './pages/products'
import Support from './pages/support'
import Login  from './pages/login'
import Register  from './pages/register'
import Forget  from './pages/forget'
import Product  from './pages/product'
import Invoice  from './pages/invoice'
import Home  from './pages/home'
import CardPayment  from './pages/card-payment'
import Policy  from './pages/policy'


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/policy',
            name: 'policy',
            component: Policy,
        },
        {
            path: '/invoice/:id',
            name: 'invoice',
            component: Invoice,
        },
        {
            path: '/card/:id',
            name: 'card',
            component: CardPayment,
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },
        {
            path: '/',
            name: 'index',
            component: Index,
        },
        {
            path: '/products',
            name: 'products',
            component: Products
        },
        {
            path: '/support',
            name: 'support',
            component: Support
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget
        },
        {
            path: '/product/:id',
            name: 'product',
            component: Product
        },

    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

