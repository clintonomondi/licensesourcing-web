function clearSession() {
    localStorage.removeItem('token');
    localStorage.removeItem('auth');
}

<!--Start of Tawk.to Script-->

    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/613dcb03d326717cb6810467/1ffcm21vv';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
})();

<!--End of Tawk.to Script-->
